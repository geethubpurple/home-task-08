# SQL Manager 2010 for MySQL 4.5.0.9
# ---------------------------------------
# Host     : localhost
# Port     : 3306
# Database : shop


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

SET FOREIGN_KEY_CHECKS=0;

DROP DATABASE IF EXISTS `shop`;

CREATE DATABASE `shop`
    CHARACTER SET 'utf8'
    COLLATE 'utf8_general_ci';

USE `shop`;

#
# Structure for the `order` table : 
#

DROP TABLE IF EXISTS `order`;

CREATE TABLE `order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hash` varchar(20) NOT NULL,
  `phone` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `hash_UNIQUE` (`hash`),
  UNIQUE KEY `hash` (`hash`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;

#
# Structure for the `order-product` table : 
#

DROP TABLE IF EXISTS `order-product`;

CREATE TABLE `order-product` (
  `orderId` int(11) NOT NULL,
  `productId` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  PRIMARY KEY (`orderId`,`productId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

#
# Structure for the `product` table : 
#

DROP TABLE IF EXISTS `product`;

CREATE TABLE `product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `price` float NOT NULL,
  `available` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

#
# Data for the `order` table  (LIMIT 0,500)
#


#
# Data for the `order-product` table  (LIMIT 0,500)
#


#
# Data for the `product` table  (LIMIT 0,500)
#

INSERT INTO `product` (`id`, `name`, `price`, `available`) VALUES 
  (1,'Microsoft Lumia 950 XL Dual Sim Black',17999,1),
  (2,'Samsung Galaxy S6 SS 32GB G920 Blue',15999,1),
  (3,'LG Google Nexus 5X 16GB Black',12444,0),
  (4,'Lenovo Vibe S1 Gold',8999,1),
  (5,'Samsung Galaxy Note 5 N920 Black',19999,0);
COMMIT;



/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;