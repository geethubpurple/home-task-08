define([
    'backbone',
    'backbone.marionette',
    './template'
], function (Backbone, Marionette, template) {

    var View = Marionette.ItemView.extend({
        template: template
    });

    return Backbone.Router.extend({

        initialize: function (options) {
            this.container = options.container;
        },

        routes: {
            'order/:hash': 'order'
        },

        order: function (hash) {
            var model = new Backbone.Model;
            model.fetch({url: '/order/' + hash}).done(function () {
                var view = new View({
                    model: model
                });
                this.container.show(view);
            }.bind(this));
        }
    });
});