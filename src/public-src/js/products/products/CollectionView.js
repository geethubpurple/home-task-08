define([
    'backbone.marionette',
    './collection-template',
    './ItemView'
], function (Marionette, template, ItemView) {
    return Marionette.CompositeView.extend({
        template: template,
        tagName: 'table',
        className: 'products',
        childView: ItemView,
        childViewContainer: 'tbody'
    });
});