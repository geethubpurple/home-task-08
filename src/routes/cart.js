var express = require('express');
var router = express.Router();
var db = require('../services/db-service');

function prepareOrderDetails(products, order) {
    var prods = Object.keys(order).map(function (key) {
        var product = products.filter(function (product) {
            return product.id == key;
        })[0];

        return {
            name: product.name,
            id: product.id,
            price: product.price,
            quantity: order[key],
            cost: order[key] * product.price
        };
    });

    return {
        products: prods,
        total: prods.reduce(function (memo, product) {
            return memo + product.cost
        }, 0)
    };
}

router.route('/')
    .get(function (req, res) {
        var data = {};
        Object.keys(req.query).forEach(function (key) {
            data[key] = parseInt(req.query[key]);
        });

        db.products(
            function (products) {
                res.json(prepareOrderDetails(products, data));
            },
            function () {
                res.sendStatus(500);
            }
        );
    })
    .post(function (req, res) {
        db.makeOrder(
            req.body.cart,
            req.body.email,
            req.body.phone,
            function (hash) {
                res.json({hash: hash})
            },
            function () {
                res.sendStatus(500);
            });
    });

module.exports = router;
