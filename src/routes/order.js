var express = require('express');
var router = express.Router();
var db = require('../services/db-service');

router.get('/:hash', function (req, res) {
    db.getOrder(
        req.params.hash,
        function (order) {
            res.json(order);
        },
        function () {
            res.sendStatus(500);
        }
    );
});

module.exports = router;