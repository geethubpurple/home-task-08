var express = require('express');
var router = express.Router();
var products = require('../data/products');
var db = require('../services/db-service');

router.get('/', function (req, res) {
    db.products(
        function (products) {
            res.json(products);
        },
        function () {
            res.sendStatus(500);
        }
    );
});

module.exports = router;
