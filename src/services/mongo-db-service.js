var MongoClient = require('mongodb').MongoClient;
var shortid = require('shortid');

var url = 'mongodb://' + (process.env.MONGODBHOST || 'localhost') + ':' + (process.env.MONGODBPORT || '27017') + '/shop';

function DBService() {
}

DBService.prototype = Object.create({
    _connect: function (success, error) {
        MongoClient.connect(url, function (err, db) {
            if (err) {
                console.error(err);
                error();
            } else {
                success(db);
            }
        });
    },

    products: function (success, error) {
        this._connect(
            function (db) {
                db.collection('product').find().toArray(function (err, products) {
                    if (err) {
                        console.error(err);
                        error();
                    } else {
                        success(products);
                    }
                    db.close();
                });
            },
            error
        );
    },

    makeOrder: function (cart, email, phone, success, error) {
        var hash = shortid.generate();
        this._connect(
            function (db) {
                db.collection('order').find().sort({orderId: -1}).limit(1).next(function (err, order) {
                    var orderId = 1;
                    if (order) {
                        orderId = order.orderId + 1;
                    }

                    db.collection('order').insertOne({
                        orderId: orderId,
                        hash: hash,
                        email: email,
                        phone: phone,
                        products: cart
                    }, function (err) {
                        if (err) {
                            console.error(err);
                            error();
                        } else {
                            success(hash);
                        }
                        db.close();
                    });
                });
            },
            error
        );
    },

    getOrder: function (hash, success, error) {
        this._connect(
            function (db) {
                db.collection('order').find({hash: hash}).limit(1).next(function (err, order) {
                    if (err) {
                        db.close();
                        console.error(err);
                        error();
                    } else {
                        db.collection('product').find({id: {$in: Object.keys(order.products).map(Number)}}).toArray(function (err, product) {
                            if (err) {
                                console.error(err);
                                error();
                            } else {
                                order.products = product.map(function (product) {
                                    return {
                                        name: product.name,
                                        price: product.price,
                                        quantity: order.products[product.id],
                                        cost: product.price * order.products[product.id]
                                    };
                                });
                                order.total = order.products.reduce(function (memo, product) {
                                    return memo + product.cost
                                }, 0);
                                success(order);
                            }
                            db.close();
                        });
                    }
                });
            },
            error
        );
    }
});

DBService.prototype.constructor = DBService;

module.exports = DBService;