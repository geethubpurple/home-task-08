var mysql = require('mysql');
var shortid = require('shortid');

var pool = mysql.createPool({
    connectionLimit: 100,
    host: process.env.MYSQLDBHOST || 'localhost',
    user: process.env.MYSQLDBUSER,
    password: process.env.MYSQLDBPASSWORD,
    database: 'shop',
    debug: false
});

function DBService() {
}

DBService.prototype = Object.create({
    _connect: function () {
        var sql = arguments[0], data = {}, success, error;

        if (arguments.length == 3) {
            success = arguments[1];
            error = arguments[2];
        } else if (arguments.length == 4) {
            data = arguments[1];
            success = arguments[2];
            error = arguments[3];
        } else {
            console.error('wrong number of arguments: ' + arguments.length);
        }

        pool.getConnection(function (err, connection) {
            if (err) {
                connection.release();
                console.error(err);
                error();
            } else {
                console.log('connected as id ' + connection.threadId);

                var query = connection.query(sql, data, function (err, rows) {
                    connection.release();
                    if (!err) {
                        success(rows);
                    } else {
                        console.error(err);
                        error();
                    }
                });

                console.log(query.sql);

                connection.on('error', function (err) {
                    connection.release();
                    console.error(err);
                    error();
                });
            }
        });
    },

    products: function (success, error) {
        this._connect('SELECT * FROM product', function (rows) {
            success(rows.map(function (product) {
                product.available = !!product.available;
                return product;
            }))
        }, error);
    },

    makeOrder: function (cart, email, phone, success, error) {
        var hash = shortid.generate(), me = this;

        var data = {email: email, phone: phone, hash: hash};
        me._connect(
            'INSERT INTO `order` SET ?',
            data,
            function () {
                me._connect(
                    'SELECT id FROM `order` WHERE hash = ?',
                    [hash],
                    function (rows) {
                        var id = rows[0].id;

                        var data = [];
                        Object.keys(cart).forEach(function (key) {
                            data.push([id, parseInt(key), cart[key]])
                        });
                        me._connect(
                            'INSERT INTO `order-product` (`orderId`, `productId`, `quantity`) VALUES ?',
                            [data],
                            function () {
                                success(hash);
                            },
                            error
                        )
                    },
                    error
                );
            },
            error
        );
    },

    getOrder: function (hash, success, error) {
        var sql =
            'SELECT p.name, p.price, op.quantity ' +
            'FROM `order` o ' +
            'INNER JOIN `order-product` op ON op.orderId = o.id ' +
            'INNER JOIN `product` p ON p.id = op.productId ' +
            'WHERE hash = ?';

        var me = this;
        me._connect(
            'SELECT * FROM `order` WHERE hash = ?',
            [hash],
            function (rows) {
                var hash = rows[0].hash, order = {
                    orderId: rows[0].id,
                    email: rows[0].email,
                    phone: rows[0].phone,
                    products: []
                };

                me._connect(
                    sql,
                    hash,
                    function (rows) {
                        rows.forEach(function (row) {
                            order.products.push({
                                name: row.name,
                                price: row.price,
                                quantity: row.quantity,
                                cost: row.price * row.quantity
                            });
                        });
                        order.total = order.products.reduce(function (memo, product) {
                            return memo + product.cost
                        }, 0);
                        success(order);
                    },
                    error
                );
            },
            error
        )
    }
});

DBService.prototype.constructor = DBService;

module.exports = DBService;